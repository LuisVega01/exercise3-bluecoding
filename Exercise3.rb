class Exercise3

  @@a = Array.new()  
  def read_array
    puts "Type the array number of elements"
    n = gets.chomp.to_i
    puts "Type the array elements"
    
    n.times do
      str = gets.chomp
      @@a.push str.to_i
    end
    @@a
  end

  def run 
    b = read_array
    result = {}
    
    b.each do |item|
      result[item] = b.select {|multiple| item % multiple == 0 && item != multiple }
    end
    result
  end
  
end

c = Exercise3.new
puts c.run

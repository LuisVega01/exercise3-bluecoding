# Challenge 3 - Arrays and Factors

This repository is the Challenge # 3 of an interview test for a Junior ROR developer position. It has a unit test using Rspec and what it does is determine  
which elements of the array are multiple of each other.

For testing it using Rspec, you just have to type in the console rspec and it will ask you for the number of elements, you can just type in 1 element and
type in 1 as that element, because the test will run the script with a 4 element array ([10,5,2,20]) and it will determine if the expected answer is the one
obtain. If you want to edit this unit testing you must change it in the Exercise3spec.rb, you will have to enter a new initial array and the expected output.  
For running the program, you just need to type in the console ruby Exercise3.rb and it will ask you for an array to examine and determine its elements factor
inside the array.

Luis Vega
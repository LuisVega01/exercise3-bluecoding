require_relative '../Exercise3'

 RSpec.describe "Exercise3" do
    it "returns the arrays multiples that are part of the array" do
      a = Exercise3.new
      allow(a).to receive(:read_array).and_return([10,5,2,20])
      expect(a.run).to eq({10=>[5,2],5=>[],2=>[],20=>[10,5,2]})
    end
end